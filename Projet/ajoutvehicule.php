<?php 
session_start();
 function dateusfr($dateus)
{
$date=explode("-",$dateus);
return $date[2]."/".$date[1]."/".$date[0];
}
function datefrus($datefr)
{
$date=explode("/",$datefr);
return $date[2]."-".$date[1]."-".$date[0];
}
$bdd= new PDO('mysql:host=localhost;dbname=reservation', 'root', '');

if (isset($_POST['formreservation'])) 
{
	 $imma= htmlspecialchars($_POST['imma']);
	   $marque=htmlspecialchars($_POST['marque']);
	    $modele=htmlspecialchars($_POST['modele']);
	    $carburant=htmlspecialchars($_POST['carburant']);
	    $ville=htmlspecialchars($_POST['ville']);
      $date_depart=$_POST['date_depart'];
      $date_retour=$_POST['date_retour'];




	if (!empty($_POST['imma']) AND !empty($_POST['marque']) AND !empty($_POST['modele'])  AND !empty($_POST['carburant']) AND !empty($_POST['ville']) AND !empty($_POST['date_depart']) AND !empty($_POST['date_retour']))
	{
    $date_depart= datefrus($date_depart);
    $date_retour= datefrus($date_retour);
	  
	    $imma=strlen($imma);
	    if ($imma <= 255) {
        if($ville <=255) {

	    	if ($marque <=255) {
	    		if($modele <=255)
	    		{


	    		if ($carburant <=255) {
	    			$requete= $bdd->prepare("INSERT INTO voiture(imma, ville, marque, modele, carburant, date_depart, date_retour) VALUES (?, ?, ?, ?, ?, ?, ?)");
	    			$requete->execute(array($imma, $ville, $marque, $modele, $carburant, $date_depart, $date_retour));
	    			$erreur ="Votre véhicule a bien été ajouté";
	    		}
	    		else
	    		{
	    			$erreur = "Votre carburant est trop long";

	    		}

	    		}
	    		else
	    		{
	    			$erreur="Votre modele est trop long";
	    		}
	    	}
	    	else
	    	{
	    		$erreur= "Votre marque est trop longue";
	    	}
	    }
      else {
        $erreur="Votre ville est trop longue";
      }
    }
	    else
	    {
	    	$erreur="Votre imma est trop long";
	    }
	}
	else
	{
		$erreur = "Tous les champs doivent être complétés";
	}
}

?>

<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Your page description here" />
  <meta name="author" content="" />

    <title>Cardoor - Car Rental HTML Template</title>

    <!--=== Bootstrap CSS ===-->
    
    <!--=== Vegas Min CSS ===-->
    <link href="assets/css/plugins/vegas.min.css" rel="stylesheet">
    <!--=== Slicknav CSS ===-->
    <link href="assets/css/plugins/slicknav.min.css" rel="stylesheet">
    <!--=== Magnific Popup CSS ===-->
    <link href="assets/css/plugins/magnific-popup.css" rel="stylesheet">
    <!--=== Owl Carousel CSS ===-->
    <link href="assets/css/plugins/owl.carousel.min.css" rel="stylesheet">
    <!--=== Gijgo CSS ===-->
    <link href="assets/css/plugins/gijgo.css" rel="stylesheet">
    <!--=== FontAwesome CSS ===-->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!--=== Theme Reset CSS ===-->
    <link href="assets/css/reset.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="style2.css" rel="stylesheet">
    <!--=== Responsive CSS ===-->
    <link href="assets/css/responsive.css" rel="stylesheet">

     <!-- css -->
  <link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="css/bootstrap.css" rel="stylesheet" />
  <link href="css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="css/flexslider.css" rel="stylesheet" />
  <link href="css/prettyPhoto.css" rel="stylesheet" />
  <link href="css/camera.css" rel="stylesheet" />
  <link href="css/jquery.bxslider.css" rel="stylesheet" />
  <link href="css/style.css" rel="stylesheet" />

  <!-- Theme skin -->
  <link href="color/default.css" rel="stylesheet" />

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="ico/favicon.png" />


    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="wrapper">

    <!-- start header -->
    <header>
      <div class="top">
        <div class="container">
          <div class="row">
            <div class="span6">
              <p class="topcontact"><i class="icon-phone"></i> 01.68.75.12.14</p>
            </div>
            <div class="span6">

              <ul class="social-network">
                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-linkedin icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-pinterest  icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Google +"><i class="icon-google-plus icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Dribbble"><i class="icon-dribbble icon-white"></i></a></li>
              </ul>

            </div>
          </div>
        </div>
      </div>
      <div class="container">


        <div class="row nomargin">
          <div class="span4">
            <div class="logo">
              <a href="index-alt3.php"><img src="img/logo.png" alt="" /></a>
            </div>
          </div>
          <div class="span8">
            <div class="navbar navbar-static-top">
              <div class="navigation">
                <nav>
                  <ul class="nav topnav">
                    <li class="dropdown active">
                      <a href="index-alt3.php"><i class="icon-home"></i> Home <i class="icon-angle-down"></i></a>
                     
                    </li>
                    
                    <li class="dropdown">
                      <a href="#">Pages <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="aboutt.php">A propos</a>
                        <li><a href="team.php">Team</a></li>
                            <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="pricingbox.php">Réserver</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                        <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="mesreservations.php">Mes réservations</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                           <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="ajoutvehicule.php">Ajouter un véhicule</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                        <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="profill.php">Profil</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                      </ul>
                    </li>
                    
                    <li class="dropdown">
                      <?php if(isset($_SESSION["id"])): ?>
                      <a href="deconnexion.php"> Se déconnecter </a>
                       <?php else: ?>
                        <a href="connexion.php"> Se connecter </a>
                        <?php endif; ?>
                      
                      
                    </li>
                    <li class="dropdown">
                      <?php if(isset($_SESSION["id"])): ?>
                     
                       <?php else: ?>
                        <a href="contact.php"> S'inscrire </a>
                        <?php endif; ?>
                    </li>
                    <li class="dropdown">
                      <?php if(isset($_SESSION["id"])): ?>
                     <span class="badge badge-secondary"><?php echo($_SESSION["pseudo"]) ?></span>
                       <?php else: ?>
                        
                        <?php endif; ?>
                    </li>
                  </ul>
                </nav>
              </div>
              <!-- end navigation -->
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- end header -->

    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="inner-heading">
              <ul class="breadcrumb">
                <li><a href="index-alt3.htmlspecialchars">Home</a> <i class="icon-angle-right"></i></li>
                <li class="active">Ajouter</li>
              </ul>
              <h2>Ajouter un véhicule!</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

    <div align="center"> 
  
  <br/>
  <br>
  <div class="container-fluid">
        <div class="container">
          <div class="row">
             

             <article class="col-md-6">
              <h3 align="center">Decrivez-nous votre voiture!</h3>
              <br>
              <br>
              
  <form method="POST" action="">
    <table>
      <tr>
      <td>
  <label for="imma">Votre immatriculation :</label> </td>
    <td><input type="text" name="imma" value="<?php if(isset($imma)) {echo $imma;} ?>"  placeholder="Entrez votre immatriculation"></td>
  </tr>
  <tr>
      <td>
  <label for="ville">Votre ville :</label> </td>
    <td><input type="text" name="ville" value="<?php if(isset($ville)) {echo $ville;} ?>"  placeholder="Entrez votre ville"></td>
  </tr>
  
    
                                      
  <tr>
      <td>

  <label for="marque">Votre marque :</label> </td>
    <td><input type="text" name="marque" value="<?php if(isset($marque)) {echo $marque;} ?>" placeholder="Entrez votre marque"></td>
  </tr>
  <tr>
      <td>
  <label for="modele">Votre modele :</label> </td>
    <td><input type="text" name="modele" value="<?php if(isset($modele)) {echo $modele;} ?>" placeholder="Entrez votre modele"></td>
  </tr>
  <tr>
      <td>
  <label for="carburant">Votre carburant :</label> </td>
    <td><input type="text" name="carburant" placeholder="Entrez votre carburant"></td>
  </tr>
  <tr>
      <td>
        <label>Date de depot</label>
      </td>
      <td>
     <div class="pick-date bookinput-item">
        <input id="startDate2" placeholder="Pick Date" name="date_depart" value="<?php if(isset($date_depart)) {echo $date_depart;} ?>" />
                                        </div> </td>
                                      </tr>
    <tr>
      <td>
        <label>Date de retour</label></td>
        <td>  <div class="retern-date bookinput-item">
                                            <input id="endDate2" placeholder="Return Date" name="date_retour" value="<?php if(isset($date_retour)) {echo $date_retour;} ?>" />
                                        </div> </td> </tr> 
    
</table>
<br>
<input class="btn btn-success" type="submit" value="Submit" name="formreservation">

</form>
<?php
    if (isset($erreur))
    {
      echo '<font color="red">'.$erreur. '</font>';
    }
    ?>
</article>


</div>
</div>
</div>

		</div>
    <footer>
      <div class="container">
        <div class="row">
         <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Gardons contact</h5>
              <address>
              <strong>Huracán company Inc.</strong><br>
              36 rue des oliviers<br>
              10000 Troyes, France
            </address>
              <p>
                <i class="icon-phone"></i> +33168751214 <br>
                <i class="icon-envelope-alt"></i> Huracán@utt.fr
              </p>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Les aéroports</h5>
              <p>
                <ul>
                  <li>Aéroport Roissy CDG</li>
                  <li>Aéroport d'Orly</li>
                  <li>Aéroport JFK New York</li>
                  <li>Aéroport Mohamed V Casablanca</li>
                  <li>Aéroport Finistère 29</li>
                </ul>
              </p>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Subscribe newsletter</h5>
              <p>
                Keep updated for new releases and freebies. Enter your e-mail and subscribe to our newsletter.
              </p>
              <form class="subscribe">
                <div class="input-append">
                  <input class="span2" id="appendedInputButton" type="text">
                  <button class="btn btn-theme" type="submit">Subscribe</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

   
      <div id="sub-footer">
        <div class="container">
          <div class="row">
            <div class="span6">
              <div class="copyright">
                <p><span>&copy; Eterna company. All right reserved</span></p>
              </div>

            </div>

            <div class="span6">
              <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Eterna
                -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bglight icon-2x active"></i></a>

  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.js"></script>

  <script src="js/modernizr.custom.js"></script>
  <script src="js/toucheffects.js"></script>
  <script src="js/google-code-prettify/prettify.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>

  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/portfolio/jquery.quicksand.js"></script>
  <script src="js/portfolio/setting.js"></script>

  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/inview.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="js/custom.js"></script>

  <script src="assets/js/jquery-3.2.1.min.js"></script>
    <!--=== Jquery Migrate Min Js ===-->
    <script src="assets/js/jquery-migrate.min.js"></script>
    <!--=== Popper Min Js ===-->
    <script src="assets/js/popper.min.js"></script>
    <!--=== Bootstrap Min Js ===-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--=== Gijgo Min Js ===-->
    <script src="assets/js/plugins/french.js"></script>
    <!--=== Vegas Min Js ===-->
    <script src="assets/js/plugins/vegas.min.js"></script>
    <!--=== Isotope Min Js ===-->
    <script src="assets/js/plugins/isotope.min.js"></script>
    <!--=== Owl Caousel Min Js ===-->
    <script src="assets/js/plugins/owl.carousel.min.js"></script>
    <!--=== Waypoint Min Js ===-->
    <script src="assets/js/plugins/waypoints.min.js"></script>
    <!--=== CounTotop Min Js ===-->
    <script src="assets/js/plugins/counterup.min.js"></script>
    <!--=== YtPlayer Min Js ===-->
    <script src="assets/js/plugins/mb.YTPlayer.js"></script>
    <!--=== Magnific Popup Min Js ===-->
    <script src="assets/js/plugins/magnific-popup.min.js"></script>
    <!--=== Slicknav Min Js ===-->
    <script src="assets/js/plugins/slicknav.min.js"></script>

    <!--=== Mian Js ===-->
    <script src="assets/js/main.js"></script>


</body>

</html>
  