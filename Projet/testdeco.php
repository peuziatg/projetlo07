<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Eterna - Professional bootstrap site template for corporate business</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Your page description here" />
  <meta name="author" content="" />

  <!-- css -->
  <link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="css/bootstrap.css" rel="stylesheet" />
  <link href="css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="css/flexslider.css" rel="stylesheet" />
  <link href="css/prettyPhoto.css" rel="stylesheet" />
  <link href="css/camera.css" rel="stylesheet" />
  <link href="css/jquery.bxslider.css" rel="stylesheet" />
  <link href="css/style.css" rel="stylesheet" />

  <!-- Theme skin -->
  <link href="color/default.css" rel="stylesheet" />

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="ico/favicon.png" />

  <!-- =======================================================
    Theme Name: Eterna
    Theme URL: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>

  <div id="wrapper">


    <!-- start header -->
    <header>
      <div class="top">
        <div class="container">
          <div class="row">
            <div class="span6">
              <p class="topcontact"><i class="icon-phone"></i> 01.68.75.12.14</p>
            </div>
            <div class="span6">

              <ul class="social-network">
                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-linkedin icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-pinterest  icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Google +"><i class="icon-google-plus icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Dribbble"><i class="icon-dribbble icon-white"></i></a></li>
              </ul>

            </div>
          </div>
        </div>
      </div>
      <div class="container">


        <div class="row nomargin">
          <div class="span4">
            <div class="logo">
              <a href="index-alt3.html"><img src="img/logo.png" alt="" /></a>
            </div>
          </div>
          <div class="span8">
            <div class="navbar navbar-static-top">
              <div class="navigation">
                <nav>
                  <ul class="nav topnav">
                    <li class="dropdown active">
                      <a href="index-alt3.html"><i class="icon-home"></i> Home <i class="icon-angle-down"></i></a>
                     
                    </li>
                    
                    <li class="dropdown">
                      <a href="#">Pages <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="aboutt.html">A propos</a>
                        <li><a href="team.html">Team</a></li>
                        <li><a href="services.html">Services</a></li>
                        <li><a href="pricingbox.html">Pricing boxes</a></li>
                        <li><a href="404.html">404</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#">Portfolio <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="portfolio-2cols.html">Portfolio 2 columns</a></li>
                        
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="deconnexion.php">Se déconnecter </a>
                      
                    </li>
                    
                  </ul>
                </nav>
              </div>
              <!-- end navigation -->
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- end header -->

    <!-- section featured -->
    <section id="featured">

      <!-- slideshow start here -->

      <div class="camera_wrap" id="camera-slide">

        <!-- slide 1 here -->
        <div data-src="img/slides/camera/slide1/6.jpg">
          <div class="camera_caption fadeFromLeft">
            <div class="container">
              <div class="row">
                <div class="span6">
                  <h2 class="animated fadeInDown"><strong>Louez des voitures au meilleur <span class="colored">prix</span></strong></h2>
                  <p class="animated fadeInUp"> Gagnez de l'argent en louant votre véhicule et faites des économies en utilisant celui d'un autre utilisateur</p>
                  <a href="pricingbox.html" class="btn btn-success btn-large animated fadeInUp">
											<i class="icon-link"></i> Réservez un véhicule
										</a>
                  
                </div>
                <div class="span6">
                  <img src="img/slides/camera/slide1/screen.png" alt="" class="animated bounceInDown delay1" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- slide 2 here -->
        <div data-src="img/slides/camera/slide2/aero.jpg">
          <div class="camera_caption fadeFromLeft">
            <div class="container">
              <div class="row">
               
                <div class="span6">
                  <h2 class="animated fadeInDown"><strong>Nous sommes présent dans plus de <span class="colored">54 aéroports</span></strong></h2>
                  <p class="animated fadeInUp"> Retrouvez dans de nombreux pays comme le Maroc, la Réunion, la France ou encore le Kenya!</p>
                  <form>
                    <a href="http://localhost/Eterna/contact.php" class="btn btn-danger btn-large animated fadeInUp">
                      <i class="icon-link"></i> En savoir plus
                    </a>
                  </form>
                </div>

              </div>
            </div>
          </div>
        </div>

        <!-- slide 3 here -->
     

      </div>

      <!-- slideshow end here -->

    </section>
    <!-- /section featured -->

    <section id="content">
      <div class="container">

        <div class="row">
          <div class="span12">
            <div class="cta-box">
              <div class="row">
                <div class="span8">
                  <div class="cta-text">
                    <h2>Réservez une voiture en <span>1 click</span> </h2>
                  </div>
                </div>
                <div class="span4">
                  <div class="cta-btn">
                    <a href="#" class="btn btn-theme">Réservez <i class="icon-angle-right"></i></a>
                  </div>
                </div>

              </div>


            </div>
          </div>
        </div>
        <div class="row">
          <div class="span12">
            <div class="row">
              <div class="span4">
                <div class="box flyLeft">
                  <div class="icon">
                    <i class="ico icon-circled icon-bgsuccess icon-money -jet icon-3x"></i>
                  </div>
                  <div class="text">
                    <h4>Best <strong>Price</strong></h4>
                    <p>
                      Nous vous garantissons des prix toujours plus bas que le marché
                    </p>
                    <a href="#">Réservez un véhicule</a>
                  </div>
                </div>
              </div>

              <div class="span4">
                <div class="box flyIn">
                  <div class="icon">
                    <i class="ico icon-circled icon-bglight icon-mobile-phone icon-3x"></i>
                  </div>
                  <div class="text">
                    <h4>Multi-<strong>plateforme</strong></h4>
                    <p>
                      Hurracan est également disponible sur tablette et smartphone!
                    </p>
                    <a href="#">Learn More</a>
                  </div>
                </div>
              </div>
              <div class="span4">
                <div class="box flyRight">
                  <div class="icon">
                    <i class="ico icon-circled icon-bgwarning icon-suitcase active icon-3x"></i>
                  </div>
                  <div class="text">
                    <h4>Bagage <strong>pris en charge</strong></h4>
                    <p>
                      Vos bagages sont pris en charge par nos services dès le dépôt de votre véhicule
                    </p>
                    <a href="#">Learn More</a>
                  </div>
                </div>
              </div>

            </div>

            <div class="row">
              <div class="span4">
                <div class="box flyLeft">
                  <div class="icon">
                    <i class="ico icon-circled icon-bgprimary icon-comments-alt icon-3x"></i>
                  </div>
                  <div class="text">
                    <h4>Service <strong>Client</strong></h4>
                    <p>
                      Nous sommes à votre écoute 24h/24, 7j/7
                    </p>
                    <a href="#">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="span4">
                <div class="box flyIn">
                  <div class="icon">
                    <i class="ico icon-circled icon-bgdanger icon-eye-open icon-3x"></i>
                  </div>
                  <div class="text">
                    <h4>Des parkings <strong>sécurisés</strong></h4>
                    <p>
                      Nos parkings sont surveillés 24h/24 et 7j/7. La bac peut intervenir à tout moment.
                    </p>
                    <a href="#">Learn More</a>
                  </div>
                </div>
              </div>
              <div class="span4">
                <div class="box flyRight">
                  <div class="icon">
                    <i class="ico icon-circled icon-bgsuccess icon-truck icon-3x"></i>
                  </div>
                  <div class="text">
                    <h4>Transfert <strong>Navette</strong></h4>
                    <p>
                      Votre transfert est 100% gratuit!
                    </p>
                    <a href="#">Learn More</a>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="row">
          <div class="span12">
            <div class="solidline"></div>
          </div>
        </div>



        <div class="row">
          <div class="span12 aligncenter">
            <h3 class="title">Ce que disent <strong>les gens</strong> sur nous</h3>
            <div class="blankline30"></div>

            <ul class="bxslider">
              <li>
                <blockquote>
                  Huracán a changé ma façon de voyager! Je peux me rendre avec mon véhicule à l'aéroport au lieu de payer un taxi et gagner de l'argent en le louant sur le parking de ce dernier ! Incroyable #Lavieestbelle #Pnl
                </blockquote>
                <div class="testimonial-autor">
                  <img src="img/dummies/testimonial/ademo.png" alt="" />
                  <h4>Ademo</h4>
                  <a href="#">www.achetele5avril.com</a>
                </div>
              </li>
              <li>
                <blockquote>
                  Qlf Anas et Gautier. Igo ils m'ont permis d'économiser jusqu'à 5000€ en louant ma clio 3 dans le parking d'Orly. 
                </blockquote>
                <div class="testimonial-autor">
                  <img src="img/dummies/testimonial/nos.png" alt="" />
                  <h4>N.O.S</h4>
                  <a href="#">www.Nospluschaudquademo?.com</a>
                </div>
              </li>
             
            </ul>

          </div>
        </div>

      </div>
    </section>


    <footer>
      <div class="container">
        <div class="row">
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Browse pages</h5>
              <ul class="link-list">
                <li><a href="#">Our company</a></li>
                <li><a href="#">Terms and conditions</a></li>
                <li><a href="#">Privacy policy</a></li>
                <li><a href="#">Press release</a></li>
                <li><a href="#">What we have done</a></li>
                <li><a href="#">Our support forum</a></li>
              </ul>

            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Get in touch</h5>
              <address>
							<strong>Eterna company Inc.</strong><br>
							Somestreet 200 VW, Suite Village A.001<br>
							Jakarta 13426 Indonesia
						</address>
              <p>
                <i class="icon-phone"></i> (123) 456-7890 - (123) 555-7891 <br>
                <i class="icon-envelope-alt"></i> email@domainname.com
              </p>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Subscribe newsletter</h5>
              <p>
                Keep updated for new releases and freebies. Enter your e-mail and subscribe to our newsletter.
              </p>
              <form class="subscribe">
                <div class="input-append">
                  <input class="span2" id="appendedInputButton" type="text">
                  <button class="btn btn-theme" type="submit">Subscribe</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div id="sub-footer">
        <div class="container">
          <div class="row">
            <div class="span6">
              <div class="copyright">
                <p><span>&copy; Eterna company. All right reserved</span></p>
              </div>

            </div>

            <div class="span6">
              <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Eterna
                -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bglight icon-2x active"></i></a>

  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.js"></script>

  <script src="js/modernizr.custom.js"></script>
  <script src="js/toucheffects.js"></script>
  <script src="js/google-code-prettify/prettify.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>
  <script src="js/camera/camera.js"></script>
  <script src="js/camera/setting.js"></script>

  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/portfolio/jquery.quicksand.js"></script>
  <script src="js/portfolio/setting.js"></script>

  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/inview.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="js/custom.js"></script>



</body>

</html>
