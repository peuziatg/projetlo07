<?php
session_start();

$bdd = new PDO('mysql:host=127.0.0.1;dbname=test', 'root', '');

if(isset($_SESSION['id'])) {
   $requser = $bdd->prepare("SELECT * FROM oui WHERE id = ?");
   $requser->execute(array($_SESSION['id']));
   $user = $requser->fetch();

   if(isset($_POST['newpseudo']) AND !empty($_POST['newpseudo']) AND $_POST['newpseudo'] != $user['pseudo']) {
      $newpseudo = htmlspecialchars($_POST['newpseudo']);
      $insertpseudo = $bdd->prepare("UPDATE oui SET pseudo = ? WHERE id = ?");
      $insertpseudo->execute(array($newpseudo, $_SESSION['id']));
      header('Location: profill.php?id='.$_SESSION['id']);
   }

   if(isset($_POST['newmail']) AND !empty($_POST['newmail']) AND $_POST['newmail'] != $user['mail']) {
      $newmail = htmlspecialchars($_POST['newmail']);

      if(filter_var($newmail, FILTER_VALIDATE_EMAIL)){ 
      $insertmail = $bdd->prepare("UPDATE oui SET mail = ? WHERE id = ?");
      $insertmail->execute(array($newmail, $_SESSION['id']));
      header('Location: profill.php?id='.$_SESSION['id']); }
      else {
            $msg="Mail invalide";
      }
   }

   if(isset($_POST['newmdp1']) AND !empty($_POST['newmdp1']) AND isset($_POST['newmdp2']) AND !empty($_POST['newmdp2'])) {
      $mdp = sha1($_POST['newmdp1']);
      $mdp2 = sha1($_POST['newmdp2']); 
      if($mdp == $mdp2) 
      {
         $insertmdp = $bdd->prepare("UPDATE oui SET mdp = ? WHERE id = ?");
         $insertmdp->execute(array($mdp, $_SESSION['id']));
         header('Location: profill.php?id='.$_SESSION['id']);
      }
       else {
         $msg = "Vos deux mdp ne correspondent pas !";
      }
   }

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Eterna - Professional bootstrap site template for corporate business</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Your page description here" />
  <meta name="author" content="" />

  <!-- css -->
  <link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="css/bootstrap.css" rel="stylesheet" />
  <link href="css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="css/flexslider.css" rel="stylesheet" />
  <link href="css/prettyPhoto.css" rel="stylesheet" />
  <link href="css/camera.css" rel="stylesheet" />
  <link href="css/jquery.bxslider.css" rel="stylesheet" />
  <link href="css/style.css" rel="stylesheet" />

  <!-- Theme skin -->
  <link href="color/default.css" rel="stylesheet" />

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="ico/favicon.png" />

  <!-- =======================================================
    Theme Name: Eterna
    Theme URL: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>

  <div id="wrapper">


    <!-- start header -->
    <header>
      <div class="top">
        <div class="container">
          <div class="row">
            <div class="span6">
              <p class="topcontact"><i class="icon-phone"></i> 01.68.75.12.14</p>
            </div>
            <div class="span6">

              <ul class="social-network">
                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-linkedin icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-pinterest  icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Google +"><i class="icon-google-plus icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Dribbble"><i class="icon-dribbble icon-white"></i></a></li>
              </ul>

            </div>
          </div>
        </div>
      </div>
      <div class="container">


        <div class="row nomargin">
          <div class="span4">
            <div class="logo">
              <a href="index-alt3.php"><img src="img/logo.png" alt="" /></a>
            </div>
          </div>
          <div class="span8">
            <div class="navbar navbar-static-top">
              <div class="navigation">
                <nav>
                  <ul class="nav topnav">
                    <li class="dropdown active">
                      <a href="index-alt3.php"><i class="icon-home"></i> Home <i class="icon-angle-down"></i></a>
                     
                    </li>
                    
                    <li class="dropdown">
                      <a href="#">Pages <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="aboutt.php">A propos</a>
                        <li><a href="team.php">Team</a></li>
                            <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="pricingbox.php">Réserver</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                        <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="mesreservations.php">Mes réservations</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                          <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="ajoutvehicule.php">Ajouter un véhicule</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                            <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="profill.php">Profil</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                        
                      </ul>
                    </li>
                    
                    <li class="dropdown">
                      <?php if(isset($_SESSION["id"])): ?>
                      <a href="deconnexion.php"> Se déconnecter </a>
                       <?php else: ?>
                        <a href="connexion.php"> Se connecter </a>
                        <?php endif; ?>
                      
                      
                    </li>
                    <li class="dropdown">
                      <?php if(isset($_SESSION["id"])): ?>
                     
                       <?php else: ?>
                        <a href="contact.php"> S'inscrire </a>
                        <?php endif; ?>
                    </li>
                     <li class="dropdown">
                      <?php if(isset($_SESSION["id"])): ?>
                     <span class="badge badge-secondary"><?php echo($_SESSION["pseudo"]) ?></span>
                       <?php else: ?>
                        
                        <?php endif; ?>
                    </li>
                  </ul>
                </nav>
              </div>
              <!-- end navigation -->
            </div>
          </div>
        </div>
      </div>
    </header>

    <div align="center">
      <div class="container-fluid">
        <div class="container">
          <div class="row">
             

             <article class="col-md-6">
              
         <h3>Edition de mon profil</h3>
         <div align="center">
            <form method="POST" action="" enctype="multipart/form-data">
              
              <div class="form-group">
            <label>Pseudo :</label> 
            <input type="text" name="newpseudo" placeholder="Pseudo" value="<?php echo $user['pseudo']; ?>" /><br /><br />
               </div>
               <div class="form-group">
               <label>Mail :</label>
               <input type="text" name="newmail" placeholder="Mail" value="<?php echo $user['mail']; ?>" /><br /><br />
             </div>
             <div class="form-group">
             
               <label>Mot de passe :</label>
               <input type="password" name="newmdp1" placeholder="Mot de passe"/><br /><br />
             </div>
             <div class="form-group">
               <label>Confirmation mdp :</label>
               <input type="password" name="newmdp2" placeholder="Confirmation du mot de passe" /><br /><br />
             </div>
             <br>
             <input  class="btn btn-success" type="submit" value="Mettre à jour" />
            </form>
            <?php if(isset($msg)) { echo $msg; } ?>
          </div>
        </article>
      </div>
    </div>
  </div>
         </div>
      </div>

      


    <footer>
      <div class="container">
        <div class="row">
         <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Gardons contact</h5>
              <address>
              <strong>Huracán company Inc.</strong><br>
              36 rue des oliviers<br>
              10000 Troyes, France
            </address>
              <p>
                <i class="icon-phone"></i> +33168751214 <br>
                <i class="icon-envelope-alt"></i> Huracán@utt.fr
              </p>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Les aéroports</h5>
              <p>
                <ul>
                  <li>Aéroport Roissy CDG</li>
                  <li>Aéroport d'Orly</li>
                  <li>Aéroport JFK New York</li>
                  <li>Aéroport Mohamed V Casablanca</li>
                  <li>Aéroport Finistère 29</li>
                </ul>
              </p>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Subscribe newsletter</h5>
              <p>
                Keep updated for new releases and freebies. Enter your e-mail and subscribe to our newsletter.
              </p>
              <form class="subscribe">
                <div class="input-append">
                  <input class="span2" id="appendedInputButton" type="text">
                  <button class="btn btn-theme" type="submit">Subscribe</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div id="sub-footer">
        <div class="container">
          <div class="row">
            <div class="span6">
              <div class="copyright">
                <p><span>&copy; Eterna company. All right reserved</span></p>
              </div>

            </div>

            <div class="span6">
              <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Eterna
                -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bglight icon-2x active"></i></a>

  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.js"></script>

  <script src="js/modernizr.custom.js"></script>
  <script src="js/toucheffects.js"></script>
  <script src="js/google-code-prettify/prettify.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>
  <script src="js/camera/camera.js"></script>
  <script src="js/camera/setting.js"></script>

  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/portfolio/jquery.quicksand.js"></script>
  <script src="js/portfolio/setting.js"></script>

  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/inview.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="js/custom.js"></script>



</body>

</html>
<?php
}
else {
  
}
?>