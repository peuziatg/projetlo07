<?php
session_start();
if(isset($_POST['ville'])) $ville=$_POST['ville'];
else      $ville="";

if(isset($_POST['date_depart']))      $date_depart=$_POST['date_depart'];
else      $date_depart="";

if(isset($_POST['date_retour']))      $date_retour=$_POST['date_retour'];
else      $date_retour="";
 
if(isset($_POST['marque']))      $marque=$_POST['marque'];
else      $marque="";
 
?>




<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Eterna - Professional bootstrap site template for corporate business</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Your page description here" />
  <meta name="author" content="" />

  <!-- css -->
  <link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="css/bootstrap.css" rel="stylesheet" />
  <link href="css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="css/flexslider.css" rel="stylesheet" />
  <link href="css/prettyPhoto.css" rel="stylesheet" />
  <link href="css/camera.css" rel="stylesheet" />
  <link href="css/jquery.bxslider.css" rel="stylesheet" />
  <link href="css/style.css" rel="stylesheet" />

  <!-- Theme skin -->
  <link href="color/default.css" rel="stylesheet" />

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="ico/favicon.png" />

  <!-- =======================================================
    Theme Name: Eterna
    Theme URL: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>

  <div id="wrapper">

    <!-- start header -->
    <header>
      <div class="top">
        <div class="container">
          <div class="row">
            <div class="span6">
              <p class="topcontact"><i class="icon-phone"></i> 01.68.75.12.14</p>
            </div>
            <div class="span6">

              <ul class="social-network">
                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-linkedin icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-pinterest  icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Google +"><i class="icon-google-plus icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Dribbble"><i class="icon-dribbble icon-white"></i></a></li>
              </ul>

            </div>
          </div>
        </div>
      </div>
      <div class="container">


        <div class="row nomargin">
          <div class="span4">
            <div class="logo">
              <a href="index-alt3.php"><img src="img/logo.png" alt="" /></a>
            </div>
          </div>
          <div class="span8">
            <div class="navbar navbar-static-top">
              <div class="navigation">
                <nav>
                  <ul class="nav topnav">
                    <li class="dropdown active">
                      <a href="index-alt3.php"><i class="icon-home"></i> Home <i class="icon-angle-down"></i></a>
                     
                    </li>
                    
                    <li class="dropdown">
                      <a href="#">Pages <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="aboutt.php">A propos</a>
                        <li><a href="team.php">Team</a></li>
                            <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="pricingbox.php">Réserver</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                        <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="mesreservations.php">Mes réservations</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                          <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="ajoutvehicule.php">Ajouter un véhicule</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                        <?php if(isset($_SESSION["id"])): ?>
                      <li><a href="profill.php">Profil</a></li>
                       <?php else: ?>
                        
                        <?php endif; ?>
                      </ul>
                    </li>
                    
                    <li class="dropdown">
                      <?php if(isset($_SESSION["id"])): ?>
                      <a href="deconnexion.php"> Se déconnecter </a>
                       <?php else: ?>
                        <a href="connexion.php"> Se connecter </a>
                        <?php endif; ?>
                      
                      
                    </li>
                    <li class="dropdown">
                      <?php if(isset($_SESSION["id"])): ?>
                     
                       <?php else: ?>
                        <a href="contact.php"> S'inscrire </a>
                        <?php endif; ?>
                    </li>
                    <li class="dropdown">
                      <?php if(isset($_SESSION["id"])): ?>
                     <span class="badge badge-secondary"><?php echo($_SESSION["pseudo"]) ?></span>
                       <?php else: ?>
                        
                        <?php endif; ?>
                    </li>
                  </ul>
                </nav>
              </div>
              <!-- end navigation -->
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- end header -->

    <?php

// On vérifie si les champs sont vides
if(empty($ville) OR empty($date_depart) OR empty($date_retour) OR empty($marque))
    {
      ?>
     <div class="row">
     <div class="col-lg-12 text-center">
      <i class="icon-circled icon-bgwarning icon-remove icon-4x"></i><br><br>
    <font color="red">Attention, veuillez remplir tous les champs</font>
  </div>
</div>
   <?php }
 
// Aucun champ n'est vide, on peut enregistrer dans la table
else 
{
 // Connexion à la base de donnée
 try
  {
  $bdd = new PDO ('mysql:host=localhost;dbname=reservation', 'root', '');
  }
 
 catch(Exception $e)
  {
  die('Erreur : '.$e->getMessage());
  }
  function dateusfr($dateus)
{
$date=explode("-",$dateus);
return $date[2]."/".$date[1]."/".$date[0];
}
function datefrus($datefr)
{
$date=explode("/",$datefr);
return $date[2]."-".$date[1]."-".$date[0];
}
$sql = "SELECT * FROM voiture WHERE ville = '".$ville."' AND date_depart = '".datefrus($date_depart)."' AND date_retour = '".datefrus($date_retour)."' AND marque='".$marque."'";
$result = $bdd->query($sql); ?>


  <?php

echo "<table>";
echo "<th>imma</th><th>ville</th><th>marque</th><th>modele</th><th>carburant</th><th>date_depart</th><th>date_retour</th>";
while($row = $result->fetch(PDO::FETCH_ASSOC)) {
echo "<tr>";
echo "<td>".$row['imma']."</td>";
echo "<td>".$row['ville']."</td>";
echo "<td>".$row['marque']."</td>";
echo "<td>".$row['modele']."</td>";
echo "<td>".$row['carburant']."</td>";
echo "<td>".dateusfr($row['date_depart'])."</td>";
echo "<td>".dateusfr($row['date_retour'])."</td>";
echo "<td><form method='POST' action='mesreservations.php'>
  
    <input type='hidden' name='ville2' value='$ville'>
    <input type='hidden' name='date_depart2' value='$date_depart'>

    <input type='hidden' name='date_retour2' value='$date_retour'>
 
 <input type='hidden' name='marque2' value='$marque'>
 
 

<input class='btn btn-success' type='submit' value='Submit' name='mesreservations'>

</form></td>";

echo "</tr>";

}
echo "</table>";


}
?>

    <footer>
          <div class="container">
        <div class="row">
         <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Gardons contact</h5>
              <address>
              <strong>Huracán company Inc.</strong><br>
              36 rue des oliviers<br>
              10000 Troyes, France
            </address>
              <p>
                <i class="icon-phone"></i> +33168751214 <br>
                <i class="icon-envelope-alt"></i> Huracán@utt.fr
              </p>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Les aéroports</h5>
              <p>
                <ul>
                  <li>Aéroport Roissy CDG</li>
                  <li>Aéroport d'Orly</li>
                  <li>Aéroport JFK New York</li>
                  <li>Aéroport Mohamed V Casablanca</li>
                  <li>Aéroport Finistère 29</li>
                </ul>
              </p>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Subscribe newsletter</h5>
              <p>
                Keep updated for new releases and freebies. Enter your e-mail and subscribe to our newsletter.
              </p>
              <form class="subscribe">
                <div class="input-append">
                  <input class="span2" id="appendedInputButton" type="text">
                  <button class="btn btn-theme" type="submit">Subscribe</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div id="sub-footer">
        <div class="container">
          <div class="row">
            <div class="span6">
              <div class="copyright">
                <p><span>&copy; Eterna company. All right reserved</span></p>
              </div>

            </div>

            <div class="span6">
              <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Eterna
                -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bglight icon-2x active"></i></a>

  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.js"></script>

  <script src="js/modernizr.custom.js"></script>
  <script src="js/toucheffects.js"></script>
  <script src="js/google-code-prettify/prettify.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>

  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/portfolio/jquery.quicksand.js"></script>
  <script src="js/portfolio/setting.js"></script>

  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/inview.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="js/custom.js"></script>


</body>

</html>
