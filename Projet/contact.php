  <?php 


$bdd= new PDO('mysql:host=localhost;dbname=test', 'root', '');

if (isset($_POST['forminscription'])) 
{
	 $pseudo= htmlspecialchars($_POST['pseudo']);
	   $mail=htmlspecialchars($_POST['mail']);
	    $mail2=htmlspecialchars($_POST['mail2']);
	    $mdp = sha1($_POST['mdp']);
	    $mdp2 = sha1($_POST['mdp2']);

	if (!empty($_POST['pseudo']) AND !empty($_POST['mail']) AND !empty($_POST['mail2'])  AND !empty($_POST['mdp']) AND !empty($_POST['mdp2']))
	{
	  
	    $pseudolength=strlen($pseudo);
	    if ($pseudolength <= 255) {

	    	if ($mail==$mail2) {
	    		if(filter_var($mail, FILTER_VALIDATE_EMAIL))
	    		{


	    		if ($mdp==$mdp2) {
	    			$requete= $bdd->prepare("INSERT INTO oui(pseudo, mail, mdp) VALUES (?, ?, ?)");
	    			$requete->execute(array($pseudo, $mail, $mdp));
	    			$erreur ="Votre compte a bien été créé <a href=\"connexion.php\">Me connecter</a>";
	    		}
	    		else
	    		{
	    			$erreur = "Vos mots de passes ne correspondent pas!";

	    		}

	    		}
	    		else
	    		{
	    			$erreur="Votre adresse mail n'est pas valide";
	    		}
	    	}
	    	else
	    	{
	    		$erreur= "<p>vos adresses mail sont différentes</p>";
	    	}
	    }
	    else
	    {
	    	$erreur="Votre pseudo est trop long";
	    }
	}
	else
	{
		$erreur = "Tous les champs doivent être complétés";
	}
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Eterna - Professional bootstrap site template for corporate business</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Your page description here" />
  <meta name="author" content="" />

  <!-- css -->
  <link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="css/bootstrap.css" rel="stylesheet" />
  <link href="css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="css/flexslider.css" rel="stylesheet" />
  <link href="css/prettyPhoto.css" rel="stylesheet" />
  <link href="css/camera.css" rel="stylesheet" />
  <link href="css/jquery.bxslider.css" rel="stylesheet" />
  <link href="css/style.css" rel="stylesheet" />

  <!-- Theme skin -->
  <link href="color/default.css" rel="stylesheet" />

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="ico/favicon.png" />

  <!-- =======================================================
    Theme Name: Eterna
    Theme URL: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>

  <div id="wrapper">

    <!-- start header -->
    <header>
      <div class="top">
        <div class="container">
          <div class="row">
            <div class="span6">
              <p class="topcontact"><i class="icon-phone"></i> 01.68.75.12.14</p>
            </div>
            <div class="span6">

              <ul class="social-network">
                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-linkedin icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-pinterest  icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Google +"><i class="icon-google-plus icon-white"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Dribbble"><i class="icon-dribbble icon-white"></i></a></li>
              </ul>

            </div>
          </div>
        </div>
      </div>
      <div class="container">


        <div class="row nomargin">
          <div class="span4">
            <div class="logo">
              <a href="index-alt3.html"><img src="img/logo.png" alt="" /></a>
            </div>
          </div>
          <div class="span8">
            <div class="navbar navbar-static-top">
              <div class="navigation">
                <nav>
                  <ul class="nav topnav">
                    <li class="dropdown">
                      <a href="index-alt3.html"><i class="icon-home"></i> Home <i class="icon-angle-down"></i></a>
                      
                    </li>
                   >
                    <li class="dropdown">
                      <a href="#">Pages <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="aboutt.html">A propos</a></li>
                        <li><a href="team.html">Team</a></li>
                      
                      </ul>
                    </li>
                    
                    <li class="dropdown">
                      <a href="connexion.php">Se connecter </a>
                    </li>
                    <li class="dropdown">
                      <a href="http://localhost/Eterna/contact.php">S'inscrire </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <!-- end navigation -->
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- end header -->

    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="inner-heading">
              <ul class="breadcrumb">
                <li><a href="index-alt3.htmlspecialchars">Home</a> <i class="icon-angle-right"></i></li>
                <li class="active">S'inscrire</li>
              </ul>
              <h2>Inscrivez-vous!</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

    <div align="center"> 
  
  <br/>
  <br>
  <div class="container-fluid">
        <div class="container">
          <div class="row">
             

             <article class="col-md-6">
              <h3 align="center">Inscription</h3>
              <br>
              <br>
              
  <form method="POST" action="">
    <table>
      <tr>
      <td>
  <label for="pseudo">Votre pseudo :</label> </td>
    <td><input type="text" name="pseudo" value="<?php if(isset($pseudo)) {echo $pseudo;} ?>"  placeholder="Entrez votre pseudo"></td>
  </tr>
  <tr>
      <td>
  <label for="mail">Votre mail :</label> </td>
    <td><input type="email" name="mail" value="<?php if(isset($mail)) {echo $mail;} ?>" placeholder="Entrez votre mail"></td>
  </tr>
  <tr>
      <td>
  <label for="mail2">Confirmation du mail :</label> </td>
    <td><input type="email" name="mail2" value="<?php if(isset($mail2)) {echo $mail2;} ?>" placeholder="Confirmer votre mail"></td>
  </tr>
  <tr>
      <td>
  <label for="mdp">Votre mdp :</label> </td>
    <td><input type="password" name="mdp" placeholder="Entrez votre mdp"></td>
  </tr>
    <tr>
      <td>
  <label for="mdp2">Confirmez votre mdp :</label> </td>
    <td><input type="password" name="mdp2" placeholder="Confirmez votre mdp"></td>
  </tr>
</table>
<br>
<input class="btn btn-success" type="submit" value="Submit" name="forminscription">

</form>
<?php
    if (isset($erreur))
    {
      echo '<font color="red">'.$erreur. '</font>';
    }
    ?>
</article>

<article class="col-md-6">
  
            <p>Si vous avez déjà un compte, connectez-vous!</p>
            <p><a class="btn btn-success btn-lg" href="connexion.php " role="button" target="_blank">Se connecter</a></p> 
            </article>
</div>
</div>
</div>

		</div>
    <footer>
      <div class="container">
        <div class="row">
         <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Gardons contact</h5>
              <address>
              <strong>Huracán company Inc.</strong><br>
              36 rue des oliviers<br>
              10000 Troyes, France
            </address>
              <p>
                <i class="icon-phone"></i> +33168751214 <br>
                <i class="icon-envelope-alt"></i> Huracán@utt.fr
              </p>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Les aéroports</h5>
              <p>
                <ul>
                  <li>Aéroport Roissy CDG</li>
                  <li>Aéroport d'Orly</li>
                  <li>Aéroport JFK New York</li>
                  <li>Aéroport Mohamed V Casablanca</li>
                  <li>Aéroport Finistère 29</li>
                </ul>
              </p>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <h5 class="widgetheading">Subscribe newsletter</h5>
              <p>
                Keep updated for new releases and freebies. Enter your e-mail and subscribe to our newsletter.
              </p>
              <form class="subscribe">
                <div class="input-append">
                  <input class="span2" id="appendedInputButton" type="text">
                  <button class="btn btn-theme" type="submit">Subscribe</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

   
      <div id="sub-footer">
        <div class="container">
          <div class="row">
            <div class="span6">
              <div class="copyright">
                <p><span>&copy; Eterna company. All right reserved</span></p>
              </div>

            </div>

            <div class="span6">
              <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Eterna
                -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bglight icon-2x active"></i></a>

  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.js"></script>

  <script src="js/modernizr.custom.js"></script>
  <script src="js/toucheffects.js"></script>
  <script src="js/google-code-prettify/prettify.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>

  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/portfolio/jquery.quicksand.js"></script>
  <script src="js/portfolio/setting.js"></script>

  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/inview.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="js/custom.js"></script>


</body>

</html>
